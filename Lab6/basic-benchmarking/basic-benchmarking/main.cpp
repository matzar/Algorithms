// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <thread>
#include <string>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;

// Define the alias "the_clock" for the clock type we're going to use.
// (You can change this to make the code below use a different clock.)
typedef std::chrono::steady_clock the_clock;

void testStuff();

//List::iterator it;
//*it;

//int& operator+(List::iterator &it) {
//	return it->data_;
//}

template <typename T> 
class List {
public:
	struct Node;
	//typedef Node *iterator;
	//iterator begin() { return head_; }
	//iterator end() { return nullptr; }

	List() : head_(nullptr) { // constructor /////

	} ////////////////////////////////////////////

	~List() { // desctructor /////////////////////
		Node* p = head_;
		std::cout << "head_ " << p << endl;
		while (p != nullptr) {
			Node* tmp = p->next_; // get the next Node
			std::cout << "temp " << tmp << endl;
			std::cout << "delete " << p << endl;
			delete p;
			p = tmp;
		}
	} /////////////////////////////////////////////

	// methods ////////////////////////////////////
	void print() {
		Node* p = head_; // pointer to the head

		while (p) { // do while p is different then 00000000
			std::cout << p->data_ << std::endl;

			p = p->next_;
		}
	}

	void pop_front() {
		if (head_) {
			Node* tmp = head_;
			head_ = tmp->next_;
			delete tmp;
		}
	}

	void push_front(T data) {
		head_ = new Node(data, head_);
	}
	//void push_front(int data) { // **diffrent version of push_front
	//	Node* node = new Node;
	//	node->data_ = data;
	//	node->next_ = head_;
	//	head_ = node;
	//}

	void push_back(T data) {
		if (head_ == nullptr) { head_ = new Node(data, head_); }
		else {
			Node* p = head_;
			while (p->next_) {
				p = p->next_;
			}
			p->next_ = new Node(data, nullptr);
		}
	}

	void pop_back() {
		if (head_ == nullptr) { return; }
		else if (head_->next_ == nullptr) {
			delete head_;
			head_ = nullptr;
		} else {
			Node* p = head_;
			Node *prev = nullptr;
			while (p->next_) {
				prev = p;
				p = p->next_;
			}
			delete p;
			prev->next_ = nullptr;

			/*Node* p = head_; // diffrent version of handling pop_back()
			while (p->next_->next_) {
				p = p->next_;
			}*/
		}
	}

	T at(int arg) {
		Node* p = head_;
		for (int i = 0; i < arg; ++i) {
			p = p->next_;
		}

		return p->data_;
	}

	T size() {
		int counter = 0;
		Node* p = head_;

		while (p) {
			counter++;

			p = (*p).next_; //  it gets the member called 'next' from the struct that 'p' points to.
		}
		return counter;
	}

	void empty() {
		head_ = nullptr;
	}

private:
	struct Node {
		Node(T data, Node *next) : data_(data), next_(next) {}
		//Node() {} // **diffrent version of push_front

		T data_;
		Node* next_;
	};

	Node* head_;  // pointer to the Node initialised to nullptr
};

int main(int argc, char *argv[])
{
	// Start timing
	the_clock::time_point start = the_clock::now();

	// Do something that takes some time
	testStuff();

	// Stop timing
	the_clock::time_point end = the_clock::now();

	// Compute the difference between the two times in milliseconds
	auto time_taken = duration_cast<milliseconds>(end - start).count();
	cout << "It took " << time_taken << " ms." << endl;

	return 0;
}

void testStuff() {
	//List *list = new List();
	List<std::string> list;

	cout << endl << endl;
	list.push_back("asd");
	list.print();
	
	cout << list.at(0);
	//list.print();
	/*cout << list.size() << endl;
	list.pop_front();
	list.push_front(100);
	list.print();

	list.empty();
	list.push_front(88);
	list.print();*/
	std::cin.get();

	return;
}