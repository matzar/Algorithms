// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>
#include <list>
#include <ctime>
#include <array>
#include <Windows.h>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;


struct Coord {
	int x, y;
	//Coord(int X, int Y) : x(X), y(Y) {}

	bool operator==(const Coord& other) {
		return x == other.x && y == other.y;
	}
	bool operator!=(const Coord& other) {
		return !(*this == other);
	}
};

std::list<Coord> path;

const int width = 10, height = 10;
int grid[width][height];

void displayGrid() {
	for (int i = 0; i < width; ++i) {
		for (int j = 0; j < height; ++j) {
			cout << grid[i][j] << "  ";
		}
		cout << endl << endl;
	}
}

void initialiseGrid() {
	for (int i = 0; i < width; ++i) {
		for (int j = 0; j < height; ++j) {
			grid[i][j] = -1;
			if (i % 2 == 0 && j % 2 == 0 && i != 0 && j != 0) {
				grid[i][j] = (-2);
			}
		}
	}
}

void checkCell(int x, int y, int value) {
	if (x < 0 || y < 0 || x >= width || y >= height) {
		return; // Not a valid position.
	}

	if (grid[x][y] == -1) {
		grid[x][y] = value;
	}
}

bool checkCell2(Coord pos) {
	if (pos.x < 0 || pos.y < 0 || pos.x >= width || pos.y >= height) {
		return; // Not a valid position.
	}

	/*if (grid[x-1][y-1] < grid[x][y] && grid[x - 1][y - 1] != (-2) && grid[x][y] != (-2)) {
	}*/
	{
		pos.x - 1, pos.y - 1
		pos.x - 1, pos.y
		pos.x - 1, pos.y + 1
		pos.x, pos.y - 1
		pos.x, pos.y + 1
		pos.x + 1, pos.y - 1
		pos.x + 1, pos.y
		pos.x + 1, pos.y + 1
	}
	path.push_back(pos);
	return true;

	return false;
}
void secondPhase(Coord start, Coord end) {
	path.push_back(end);
	while (path.back() != start) {
		Coord pos = path.back();
		
		system("cls");
		cout << "Current: " << pos.x << "," << pos.y << endl;
		cout << "Target: " << start.x << "," << start.y << endl;
		displayGrid();
		for (auto it = path.begin(); it != path.end(); ++it) {
			Coord temp = *it;
			cout << temp.x << " " << temp.y << endl;
		}
		std::cin.get();

		if (checkCell2(pos)) continue;
		else break;
	}
	cout << start.x << " " << start.y << endl;
}
inline void delay(unsigned long ms) {
	Sleep(ms);
}

void firstPhase(Coord start, Coord end) {
	grid[start.x][start.y] = 0; // write 0 into the starting cell
	if (grid[end.x][end.y] != -1) { grid[end.x][end.y] = -1; };

	for (int loop = 0;; ++loop) {
		system("cls");
		displayGrid();
		//std::cin.get();

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				if (grid[i][j] == loop) {
					checkCell(i - 1, j - 1, loop + 1);
					checkCell(i - 1, j, loop + 1);
					checkCell(i - 1, j + 1, loop + 1);
					checkCell(i, j - 1, loop + 1);
					checkCell(i, j + 1, loop + 1);
					checkCell(i + 1, j - 1, loop + 1);
					checkCell(i + 1, j, loop + 1);
					checkCell(i + 1, j + 1, loop + 1);
					if (i == end.x && j == end.y)
						return;
				}
			}
		}
	}
}

int main(int argc, char *argv[]) {
	srand(static_cast<unsigned int>(time(NULL)));

	Coord start{ 0, 0 };
	Coord end{ 4, 4 };
	initialiseGrid();
	firstPhase(start, end);
	secondPhase(start, end);

	std::cin.get();

	return 0;
}

